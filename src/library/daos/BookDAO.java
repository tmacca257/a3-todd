package library.daos;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import library.interfaces.entities.IBook;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.daos.BookHelper;



public class BookDAO implements IBookDAO
{
	//variables
	private int nextID_;
	private IBook book_;
	private IBookHelper helper_;
	private ArrayList<IBook> bookList_ = new ArrayList<IBook>();
	
	
	
	//constructor
	public BookDAO(IBookHelper helper) throws IllegalArgumentException
	{
		if(helper == null)
		{
			throw new IllegalArgumentException("helper is null");
		}
		this.helper_ = helper;
		this.nextID_ = 1;
	}
	
	
	
	public IBook addBook(String author, String title, String callNumber)
	{		
		//uniqueId_ = (int) (System.currentTimeMillis() & 0xfffffff);
		int id = getNextId();
		book_ = helper_.makeBook(author, title, callNumber, id);
		
		bookList_.add(book_);
		return book_;
	}



	private int getNextId() {
		return nextID_++;
	}



	@Override
	public IBook getBookByID(int id)
	{
		Iterator<IBook> iterator = bookList_.iterator();
		while (iterator.hasNext())
		{
			IBook book = iterator.next();
			if(book.getID() == id)
				{
					//found it!
					this.book_ = book;
				}
		}
		if (book_.getID()== id)
		{
			return book_;
		}
		else
		{
			return null;
		}
		
	}



	@Override
	public ArrayList<IBook> listBooks()
	{
		return this.bookList_;
	}



	@Override
	public List<IBook> findBooksByAuthor(String author)
	{
		ArrayList<IBook> bookListByAuthor = new ArrayList<IBook>();
				
		Iterator<IBook> iterator = bookList_.iterator();
		while (iterator.hasNext())
		{
			IBook book = iterator.next();
			if(book.getAuthor() == author)
				{
					bookListByAuthor.add(book);
				}
		}
		return bookListByAuthor;
	}



	@Override
	public List<IBook> findBooksByTitle(String title)
	{
		ArrayList<IBook> bookListByTitle = new ArrayList<IBook>();
		
		Iterator<IBook> iterator = bookList_.iterator();
		while (iterator.hasNext())
		{
			IBook book = iterator.next();
			if(book.getTitle() == title)
				{
					bookListByTitle.add(book);
				}
		}
		return bookListByTitle;
	}



	@Override
	public List<IBook> findBooksByAuthorTitle(String author, String title)
	{
		ArrayList<IBook> bookListByAuthorTitle = new ArrayList<IBook>();
		
		Iterator<IBook> iterator = bookList_.iterator();
		while (iterator.hasNext())
		{
			IBook book = iterator.next();
			if(book.getAuthor() == author && book.getTitle() == title)
				{
				bookListByAuthorTitle.add(book);
				}
		}
		return bookListByAuthorTitle ;
	}
	
	
	
	@Override
	public String toString() {
		return (String.format("Id:  %d\nAuthor:   %s\nTitle:    %s\nCall Number: %s", 
				book_.getID(), book_.getAuthor(), book_.getTitle(), book_.getCallNumber()));
	}
}