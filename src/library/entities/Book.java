package library.entities;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;

public class Book implements IBook {

	//variables
	private String author_;
	private String title_;
	private String callNumber_;
	private Integer id_;
	private ILoan loan_;
	private EBookState state_;
	
	
	
	//constructor
	public Book (String author, String title, String callNumber, Integer bookID) throws IllegalArgumentException
	{
		if(author == null || author.isEmpty() || title == null || title.isEmpty() || callNumber == null 
				|| callNumber.isEmpty() || bookID <= 0)
		{
			throw new IllegalArgumentException("Author, Title & Call Number must not be blank or BookID is <= 0");
		}
		this.author_ = author;
		this.title_ = title;
		this.callNumber_ = callNumber;
		this.id_ = bookID;	
		this.state_ = EBookState.AVAILABLE;
	}
	
	
	
	//associates the loan with the book 
	public void borrow(ILoan loan)
	{
		if 	(this.state_ == EBookState.AVAILABLE) 
		{
			this.loan_ = loan;
		}
		else
		{
			throw new RuntimeException("The book is not currently AVAILABLE") ;
		}
		
	}
	
	
	
	//returns the loan associated with the book 
	public ILoan getLoan()
	{		
		if (this.state_ == EBookState.ON_LOAN) 
		{
			return this.loan_;
		}
		return this.loan_;
	}
	
	

	//removes the loan the book is associated with 
	public void returnBook(boolean damaged)
	{		
		if(state_ == EBookState.ON_LOAN)
			{
				
				if(damaged == true)
				{
					this.state_ = EBookState.DAMAGED;
				}
				else
				{
					this.state_ = EBookState.AVAILABLE;
				}
			}
		else
		{
			throw new RuntimeException("the book is not currently ON_LOAN");
		}
	}
	
	
	
	public void lose ()
	{
		if(state_ == EBookState.ON_LOAN)
		{
			this.state_ = EBookState.LOST;
		}
		else
		{
			throw new RuntimeException("the book is not currently ON_LOAN");
		}
	}
	
	
	
	public void repair()
	{
		if(state_ == EBookState.DAMAGED)
		{
			this.state_ = EBookState.AVAILABLE;
		}
		else
		{
			throw new RuntimeException("the book is not currently DAMAGED");
		}
	}
	
	
	
	public void dispose()
	{
		if(state_ == EBookState.AVAILABLE || state_ == EBookState.DAMAGED || state_ == EBookState.LOST)
		{
			this.state_ = EBookState.DISPOSED;
		}
		else
		{
			throw new RuntimeException("the book is not currently AVAILABLE, DAMAGED, or LOST");
		}
	}
	
	
	
	public EBookState getState()
	{
		return this.state_;
	}
	
	
	
	public void setState(EBookState newState)
	{
		this.state_ = newState;
	}
	
	
	
	public String getAuthor()
	{
		return this.author_;
	}
	
	
	
	public String getTitle()
	{
		return this.title_;
	}
	
	
	
	public String getCallNumber()
	{
		return this.callNumber_;
	}
	
	
	
	public int getID()
	{
		return this.id_;
	}
}