package library;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import library.interfaces.EBorrowState;
import library.interfaces.IBorrowUI;
import library.interfaces.IBorrowUIListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.daos.IMemberHelper;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.ICardReaderListener;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.interfaces.hardware.IScannerListener;


public class BorrowUC_CTL implements ICardReaderListener, 
									 IScannerListener, 
									 IBorrowUIListener {
	
	private ICardReader reader_;
	private IScanner scanner_; 
	private IPrinter printer_; 
	private IDisplay display_;
	//private String state;
	private int scanCount_ = 0;
	private IBorrowUI ui_;
	private EBorrowState state_; 
	private IBookDAO bookDAO_;
	private IMemberDAO memberDAO_;
	private ILoanDAO loanDAO_;
	private List<IBook> bookList_;
	private List<ILoan> loanList_;
	private IMember borrower_;
	private IBook book_;
	private Integer checkBarcode_ = 0;
	
	private JPanel previous_;

	
	
	public BorrowUC_CTL(ICardReader reader, IScanner scanner, IPrinter printer, IDisplay display,
			IBookDAO bookDAO, ILoanDAO loanDAO, IMemberDAO memberDAO)
	{
		this.reader_ = reader;
		this.scanner_ = scanner;
		this.printer_ = printer;
		this.bookDAO_ = bookDAO;
		this.loanDAO_ = loanDAO;
		this.memberDAO_ = memberDAO;
		this.display_ = display;
		this.ui_ = new BorrowUC_UI(this);
		state_ = EBorrowState.CREATED;
		
		reader.addListener(this);
		scanner.addListener(this);
	}
	
	
	
	public BorrowUC_CTL(ICardReader reader, IScanner scanner, 
			IPrinter printer, IDisplay display,
			IBookDAO bookDAO, ILoanDAO loanDAO, IMemberDAO memberDAO, BorrowUC_UI ui)
	{
		this.reader_ = reader;
		this.scanner_ = scanner;
		this.printer_ = printer;
		this.bookDAO_ = bookDAO;
		this.loanDAO_ = loanDAO;
		this.memberDAO_ = memberDAO;
		this.display_ = display;
		this.ui_ = ui;
		state_ = EBorrowState.CREATED;
		
		reader.addListener(this);
		scanner.addListener(this);
	}



	public void initialise()
	{
		previous_ = display_.getDisplay();
		display_.setDisplay ((JPanel) ui_, "Borrow UI");		
		reader_.setEnabled(true);
		scanner_.setEnabled(false);
		state_ = EBorrowState.INITIALIZED;
		ui_.setState(state_);
	}
	
	
	
	public void close()
	{
		display_.setDisplay(previous_, "Main Menu");
	}

	
	
	@Override
	public void cardSwiped(int memberID)
	{
		//check if its in the correct state
		if(!state_.equals(EBorrowState.INITIALIZED))
		{
			throw new RuntimeException(
					String.format("BorrowUC_CTL : cardSwiped : illegal operation in state: %s", state_));
		}
		
		//get borrower
		borrower_ = memberDAO_.getMemberByID(memberID);
		
		if (borrower_ == null)
		{
			ui_.displayErrorMessage(String.format("Member ID %d not found", memberID));
			return;
		}
		
		//get restrictions of the borrower
		boolean finesPayable = borrower_.hasFinesPayable();
		boolean overDueLoans = borrower_.hasOverDueLoans();
		boolean reachedFineLimit = borrower_.hasReachedFineLimit();
		boolean reachedLoanLimit = borrower_.hasReachedLoanLimit();
		boolean borrowingRestricted = (overDueLoans || reachedLoanLimit || reachedFineLimit);
		
		if (borrowingRestricted)
		{
			reader_.setEnabled(false);
			scanner_.setEnabled(false);
			state_ = EBorrowState.BORROWING_RESTRICTED;
			ui_.setState(state_);
			ui_.displayErrorMessage(String.format("Member %d cannot borrow at this time.", borrower_.getID()));
		}
		else
		{
			scanner_.setEnabled(true);
			reader_.setEnabled(false);
			state_ = EBorrowState.SCANNING_BOOKS;
			ui_.setState(state_);
			bookList_ = new ArrayList<IBook>();
			loanList_ = new ArrayList<ILoan>();
			scanCount_ = borrower_.getLoans().size();
			
			//clear book display
			ui_.displayScannedBookDetails("");
			//clear loan display
			ui_.displayPendingLoan("");
		}
		
		//show borrower details
		int borrowerID = borrower_.getID();
		String borrowerName = borrower_.getFirstName() + " " + borrower_.getLastName();
		String borrowerPhone = borrower_.getContactPhone();
		ui_.displayMemberDetails(borrowerID, borrowerName, borrowerPhone);
		
		if(finesPayable)
		{
			//display message
			float amountOwing = borrower_.getFineAmount();
			ui_.displayOutstandingFineMessage(amountOwing);
		}
		
		if(overDueLoans)
		{
			//display message
			ui_.displayOverDueMessage();
		}
		
		if(reachedFineLimit)
		{
			//display message
			float amountOwing = borrower_.getFineAmount();
			ui_.displayOverFineLimitMessage(amountOwing);
		}
		
		if(reachedLoanLimit)
		{
			//display message
			ui_.displayAtLoanLimitMessage();
		}
		
		//display all existing loans
		String existingLoans = buildLoanListDisplay(borrower_.getLoans());
		ui_.displayExistingLoan(existingLoans);
	}
	
	
	
	@Override
	public void bookScanned(int barcode)
	{
		//intent - Add book to list of intended/pending loans
		//check if its in the correct state
		if(!state_.equals(EBorrowState.SCANNING_BOOKS))
		{
			throw new RuntimeException(
					String.format("BorrowUC_CTL : bookScanned : illegal operation in state: %s", state_));
		}
		
		//get book
		book_ = bookDAO_.getBookByID(barcode);
		
		if (book_ == null)
		{
			ui_.displayErrorMessage(String.format("Book %d not found", barcode));
			return;
		}
		
		if (!book_.getState().equals(EBookState.AVAILABLE))
		{
			//book not available
			System.out.println("bookstate");
			reader_.setEnabled(false);
			scanner_.setEnabled(true);
			state_ = EBorrowState.SCANNING_BOOKS;
			ui_.setState(state_);
			ui_.displayErrorMessage(String.format("Book %d is not AVAILABLE at this time.", book_.getState()));
		}
		
		else if(checkBarcode_.equals(barcode))
		{
			//(book already scanned)
			reader_.setEnabled(false);
			scanner_.setEnabled(true);
			state_ = EBorrowState.SCANNING_BOOKS;
			ui_.setState(state_);
			ui_.displayErrorMessage(String.format("Book %d already scanned.", book_.getID()));			
		}

		else if(scanCount_ < IMember.LOAN_LIMIT)
		{
			reader_.setEnabled(false);
			scanner_.setEnabled(true);
			
			//display book details
			reader_.setEnabled(false);
			scanner_.setEnabled(true);
			String bookDetails =  bookDAO_.toString();
			ui_.displayScannedBookDetails(bookDetails);
			
			//create new pending loan			
			ILoan pendingLoan = loanDAO_.createLoan(borrower_, book_);
			loanList_.add(pendingLoan);
			
			//display pending loans
			String pendingLoans = buildPendingLoanListDisplay(loanList_);
			
			ui_.displayPendingLoan(pendingLoans);
			state_ = EBorrowState.SCANNING_BOOKS;
			ui_.setState(state_);
			this.scanCount_++;
		}
		
		else if(scanCount_ == IMember.LOAN_LIMIT)
		{
			reader_.setEnabled(false);
			scanner_.setEnabled(false);
			state_ = EBorrowState.CONFIRMING_LOANS;
			ui_.setState(state_);
			
			String pendingLoans = buildPendingLoanListDisplay(loanList_);
			ui_.displayConfirmingLoan(pendingLoans);
			this.scanCount_++;
		}
		checkBarcode_ = barcode;
	}

	
	
	public void setState(EBorrowState state)
	{
		this.state_ = state;
	}
	
	
	
	public EBorrowState getState()
	{
		return this.state_;
	}
	
	public int getScanCount()
	{
		return this.scanCount_;
	}

	
	
	@Override
	public void cancelled()
	{
		close();
	}
	
	
	
	@Override
	public void scansCompleted()
	{
		//check if its in the correct state
		if(!state_.equals(EBorrowState.SCANNING_BOOKS))
		{
			throw new RuntimeException(
					String.format("BorrowUC_CTL : scansCompleted : illegal operation in state: %s", state_));
		}
		
		reader_.setEnabled(false);
		scanner_.setEnabled(false);
		state_ = EBorrowState.CONFIRMING_LOANS;
		ui_.setState(state_);
		
		String pendingLoans = buildPendingLoanListDisplay(loanList_);
		ui_.displayConfirmingLoan(pendingLoans);
	}

	
	
	@Override
	public void loansConfirmed()
	{
		//check if its in the correct state
		if(!state_.equals(EBorrowState.CONFIRMING_LOANS))
		{
			throw new RuntimeException(
					String.format("BorrowUC_CTL : loansConfirmed : illegal operation in state: %s", state_));
		}
				
		reader_.setEnabled(false);
		scanner_.setEnabled(false);
		
		//commit and record all pending loans
		commitListOfLoans(loanList_);
		
		String commitedLoans = buildPendingLoanListDisplay(loanList_);
		//loan slip of committed loans printed
		printer_.print(commitedLoans);
		
		//display main menu
		display_.setDisplay(previous_, "Main Menu");
		state_ = EBorrowState.COMPLETED;
		ui_.setState(state_);
	}

	
	
	@Override
	public void loansRejected() {
		//check if its in the correct state
		if(!state_.equals(EBorrowState.CONFIRMING_LOANS))
		{
			throw new RuntimeException(
					String.format("BorrowUC_CTL : loansRejected : illegal operation in state: %s", state_));
		}
				
		reader_.setEnabled(false);
		scanner_.setEnabled(true);
		state_ = EBorrowState.SCANNING_BOOKS;
		ui_.setState(state_);
			
		//Existing loan details displayed
		String existingLoans = buildLoanListDisplay(borrower_.getLoans());
		ui_.displayExistingLoan(existingLoans);
		
		//empty pending loan list
		loanList_ = new ArrayList<ILoan>();
		
		//scan count = number of existing loans
		this.scanCount_ = borrower_.getLoans().size();
		
		//clear book display
		ui_.displayScannedBookDetails("");
		//clear loan display
		ui_.displayPendingLoan("");
		
		checkBarcode_ = 0; //set back to zero
	}

	
	
	private void commitListOfLoans(List<ILoan> loans)
	{
		for (ILoan ln : loans) 
		{
			loanDAO_.commitLoan(ln);
		}	
	}
	
	
	
	private String buildPendingLoanListDisplay(List<ILoan> loans)
	{
		StringBuilder bld = new StringBuilder();
		for (ILoan ln : loans)
		{
			if (bld.length() > 0) bld.append("\n\n");
			bld.append(ln.toString());
		}
		return bld.toString();		
	}
	
	
	
	private String buildLoanListDisplay(List<ILoan> loans)
	{
		StringBuilder bld = new StringBuilder();
		for (ILoan ln : loans)
		{
			if (bld.length() > 0) bld.append("\n\n");
			bld.append(ln.toString());
		}
		return bld.toString();		
	}
}