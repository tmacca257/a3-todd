package unitTesting;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

import library.daos.BookDAO;
import library.daos.BookHelper;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BookDAOTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	//@Mock
	
	IBookHelper helper_;
	
	//initilise variables
	private IBookDAO sut_;
	
	private String author_;
	private String title_;
	private String callNumber_;
	
	private String author2_;
	private String title2_;
	private String callNumber2_;
	
	@Before
	public void setup() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		
		author_ = "John Smith";
		title_ = "My new book";
		callNumber_ = "123";
		
		author2_ = "Jane Jones";
		title2_ = "River Sky";
		callNumber2_ = "3344f";
		
		//IBookHelper helper_ = mock(IBookHelper.class); this mock stopped working once i integrated?
		helper_ = new BookHelper();
		sut_ = new BookDAO(helper_);
	}
	
	@After
	public void tearDown() throws Exception{
	}
	
	
	@Test
	public void testGoodConstructor()
	{			
		assertTrue(sut_ instanceof IBookDAO);
	}

	@Test
	public void testBadConstructorHelperIsNULL()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("helper is null"));
		
		//execute
		new BookDAO(null);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	
	@Test
	public void testAddBook()
	{
		sut_.addBook(author_, title_, callNumber_);
		
		//assert
		assertTrue(sut_ instanceof IBookDAO);
	}

	@Test
	public void testGetBookByIDWithValidID()
	{
		//arrange
		IBook book = sut_.addBook(author_, title_, callNumber_);
		
		//execute
		int bookId = book.getID(); //get the bookid to check
		IBook checkBook = sut_.getBookByID(bookId);
		
		//assert
		assertEquals(book, checkBook);
		assertTrue(checkBook instanceof IBook);
	}
	
	@Test
	public void testGetBookByIDWithValidID2ndBook()
	{
		//arrange
		IBook book = sut_.addBook(author2_, title2_, callNumber2_);
		
		//execute
		int bookId = book.getID(); //get the bookid to check
		IBook checkBook = sut_.getBookByID(bookId);
		
		//assert
		assertEquals(book, checkBook);
		assertTrue(checkBook instanceof IBook);
		assertTrue(checkBook !=null);		
	}
	
	@Test
	public void testGetBookByIDWithNONValidID()
	{
		//arrange
		sut_.addBook(author_, title_, callNumber_); //adds something to the list
				
		//execute
		IBook checkBook = sut_.getBookByID(2); //enter invalid id 2
		
		//assert
		assertEquals(null, checkBook); //check if returned null
	}
	
	@Test
	public void testListBooks()
	{
		//arrange
		sut_.addBook(author_, title_, callNumber_); //adds something to the list
		
		//execute
		List<IBook> checkBook = sut_.listBooks();
		
		//assert
		assertTrue(checkBook instanceof List);
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testListBooks2ndBook()
	{
		//arrange
		sut_.addBook(author2_, title2_, callNumber2_); //adds something to the list
		
		//execute
		List<IBook> checkBook = sut_.listBooks();
		
		//assert
		assertTrue(checkBook instanceof List);
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByAuthor()
	{
		//arrange
		sut_.addBook(author_, title_, callNumber_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthor(author_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByAuthor2ndBook()
	{
		//arrange
		sut_.addBook(author2_, title2_, callNumber2_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthor(author2_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByAuthorReturnedEmptyList()
	{
		//arrange
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthor(author_);
				
		//assert
		//check if the list returned is empty
		assertTrue(checkBook.isEmpty());
		assertTrue(checkBook instanceof List); //check if a list
	}
	
	@Test
	public void testFindBooksByTitle()
	{
		//arrange
		sut_.addBook(author_, title_, callNumber_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByTitle(title_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByTitle2ndBook()
	{
		//arrange
		sut_.addBook(author2_, title2_, callNumber2_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByTitle(title2_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByTitleReturnedEmptyList()
	{
		//arrange
				
		//execute
		List<IBook> checkBook = sut_.findBooksByTitle(title_);
				
		//assert
		//check if the list returned is empty
		assertTrue(checkBook.isEmpty());
		assertTrue(checkBook instanceof List); //check if a list
	}
	
	@Test
	public void testFindBooksByAuthorTitle()
	{
		//arrange
		sut_.addBook(author_, title_, callNumber_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthorTitle(author_, title_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByAuthorTitle2ndBook()
	{
		//arrange
		sut_.addBook(author2_, title2_, callNumber2_);
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthorTitle(author2_, title2_);
				
		//assert
		assertTrue(checkBook instanceof List); //check if a list
		assertFalse(checkBook.isEmpty()); //check if not empty
	}
	
	@Test
	public void testFindBooksByAuthorTitleReturnedEmptyList()
	{
		//arrange
				
		//execute
		List<IBook> checkBook = sut_.findBooksByAuthorTitle(author_, title_);
				
		//assert
		//check if the list returned is empty
		assertTrue(checkBook.isEmpty());
		assertTrue(checkBook instanceof List); //check if a list
	}
	
	@Test
	public void testFindBooksByAuthorTitleReturnedEmptyListWhenInvalidAuthorTitleEntered()
	{
		//arrange
		sut_.addBook(author2_, title2_, callNumber2_);
				
		//execute
		//this is searching for author but title2 was written by author2 and not author
		List<IBook> checkBook = sut_.findBooksByAuthorTitle(author_, title2_);
				
		//assert
		//check if the list returned is empty
		assertTrue(checkBook.isEmpty());
		assertTrue(checkBook instanceof List); //check if a list
	}
}