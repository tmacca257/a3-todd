package unitTesting;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.entities.Book;

public class BookTest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Mock
	ILoan loan;
		
	//initilise variables
	private IBook sut_;
	private String author_;
	private String title_;
	private String callNumber_;
	private Integer bookId_;

	
	@Before
	public void setup() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		
		author_ = "John Smith";
		title_ = "My new book";
		callNumber_ = "123";
		bookId_ = 1;
		
		sut_ = new Book(author_, title_, callNumber_, bookId_);
	}
	
	@After
	public void tearDown() throws Exception{
	}
	
	@Test
	public void testGoodConstructor()
	{
		assertTrue(sut_ instanceof IBook);
	}
	
	@Test
	public void testBadConstructorBookAuthorNull()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(null, title_, callNumber_, bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookAuthorBlank()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book("", title_, callNumber_, bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookTitleNull()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, null, callNumber_, bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookTitleBlank()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, "", callNumber_, bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookCallNumberNull()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, title_, null, bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookCallNumberBlank()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, title_, "", bookId_);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookIDIsLessThanZero()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, title_, callNumber_, -1);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBadConstructorBookIDIsEqualToZero()
	{
		//arrange
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(containsString("Author, Title & Call Number must not be blank or BookID is <= 0"));
		
		//execute
		sut_ = new Book(author_, title_, callNumber_, 0);
		
		//assert
		fail("Constructor should have thrown IllegalArgumentException");
	}
	
	@Test
	public void testBorrowBookStateIsAVAILABLE()
	{
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set the state for the test
		//execute
		
		sut_.borrow(loan);
		//assert		
		assertTrue(sut_.getState() == EBookState.AVAILABLE);
	}
	
	@Test
	public void testBorrowBookStateIsNotAVAILABLE()
	{
		//arrange
		sut_.setState(EBookState.LOST); //set the state to not equal available for test
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("The book is not currently AVAILABLE"));
		
		//execute
		sut_.borrow(loan);
		
		//assert		
		assertTrue(sut_.getState() != EBookState.AVAILABLE);
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testGetLoanIsOnLoan()
	{
		//testing to check if the Loan is ON_LOAN
		//arrange
		sut_.setState(EBookState.ON_LOAN); //set the state to ON_LOAN for test
		
		//execute
		sut_.getLoan();
		
		//assert
		assertTrue(sut_.getState() == EBookState.ON_LOAN);
	}
	
	@Test
	public void testGetLoanIsNotOnLoan()
	{
		//testing to check if the Loan is ON_LOAN
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set the state to be not ON_LOAN for test
		
		//execute
		sut_.getLoan();
		
		//assert
		assertTrue(sut_.getState() != EBookState.ON_LOAN);
	}
	
	@Test
	public void testReturnBookDamagedIfOnLoan()
	{
		//arrange
		sut_.setState(EBookState.ON_LOAN); //set the state for the test
		
		//execute
		sut_.returnBook(true); //returning book damaged
		
		//assert		
		assertTrue(sut_.getState() == EBookState.DAMAGED);
	}
	
	@Test
	public void testReturnBookNotDamagedIfOnLoan()
	{
		//arrange
		sut_.setState(EBookState.ON_LOAN); //set the state for the test
		
		//execute
		sut_.returnBook(false); //returning book not damaged
		
		//assert		
		assertTrue(sut_.getState() == EBookState.AVAILABLE);
	}
	
	@Test
	public void testReturnBookIfNotOnLoan()
	{
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set the state to not ON_LOAN
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("the book is not currently ON_LOAN"));
		
		//execute
		sut_.returnBook(false); //returning book not damaged
		
		//assert		
		assertTrue(sut_.getState() != EBookState.ON_LOAN);
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testLoseIfOnLoan()
	{
		//arrange
		sut_.setState(EBookState.ON_LOAN); //set the state for the test
		
		//execute
		sut_.lose();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.LOST);
	}
	
	@Test
	public void testLoseIfNotOnLoan()
	{
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set the state to not ON_LOAN
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("the book is not currently ON_LOAN"));
		
		//execute
		sut_.lose();	
		
		//assert
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testRepairIfDamaged()
	{
		//arrange
		sut_.setState(EBookState.DAMAGED); //set the state for the test
		
		//execute
		sut_.repair();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.AVAILABLE);
	}
	
	@Test
	public void testRepairIfNotDamaged()
	{
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set to not damaged
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("the book is not currently DAMAGED"));
		
		//execute
		sut_.repair();	
		
		//assert
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testDisposeIfAvailable()
	{
		//arrange
		sut_.setState(EBookState.AVAILABLE); //set the state for the test
		
		//execute
		sut_.dispose();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.DISPOSED);
	}
	
	@Test
	public void testDisposeIfDamaged()
	{
		//arrange
		sut_.setState(EBookState.DAMAGED); //set the state for the test
		
		//execute
		sut_.dispose();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.DISPOSED);
	}
	
	@Test
	public void testDisposeIfLost()
	{
		//arrange
		sut_.setState(EBookState.LOST); //set the state for the test
		
		//execute
		sut_.dispose();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.DISPOSED);
	}
	
	@Test
	public void testDisposeIfNotCurrentlyAvailableOrDamagedOrLost()
	{
		//arrange
		sut_.setState(EBookState.ON_LOAN); //set the state for the test
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("the book is not currently AVAILABLE, DAMAGED, or LOST"));
		
		//execute
		sut_.dispose();
		
		//assert
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testGetState()
	{
		//arrange
		sut_.setState(EBookState.LOST); //set the state for the test
		//execute
		sut_.getState();
		
		//assert		
		assertTrue(sut_.getState() == EBookState.LOST);
	}
	
	@Test
	public void testSetState()
	{
		//execute
		sut_.setState(EBookState.AVAILABLE);
		
		//assert		
		assertTrue(sut_.getState() == EBookState.AVAILABLE);
	}
	
	@Test
	public void testGetAuthor()
	{
		//execute
		String result = sut_.getAuthor();
		
		//assert
		assertNotNull(result); //check if not null
		assertEquals("John Smith", result); //check = John Smith
	}
	
	@Test
	public void testGetTitle()
	{
		//execute
		String result = sut_.getTitle();
		
		//assert
		assertNotNull(result); //check if not null
		assertEquals("My new book", result); // check = My new book
	}
	
	@Test
	public void testGetCallNumber()
	{
		//execute
		String result = sut_.getCallNumber();
		
		//assert
		assertNotNull(result); //check if not null
		assertEquals("123", result); // check = 123
	}
	
	@Test
	public void testGetId()
	{
		//execute
		int result = sut_.getID();
		//assert
		assertNotNull(result); //check if not null
		assertEquals(1, result); // check = 1
	}
}