package unitTesting;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;
import library.daos.BookHelper;

public class BookHelperTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	//initilise variables
	private IBookHelper sut_;
	private String author_;
	private String title_;
	private String callNumber_;
	private Integer id_;
	private String author2_;
	private String title2_;
	private String callNumber2_;
	private Integer id2_;
	
	@Before
	public void setup() throws Exception 
	{
		author_ = "John Smith";
		title_ = "My new book";
		callNumber_ = "123";
		id_ = 1;
		
		author2_ = "Jane Jones";
		title2_ = "River Sky";
		callNumber2_ = "3344f";
		id2_ = 2;
		
		sut_ = new BookHelper();
	}
	
	@Test
	public void testMakeBook()
	{
		//execute
		IBook book = sut_.makeBook(author_, title_, callNumber_, id_);
		
		//assert
		assertTrue(book.getID() == 1); //verifys that it returns the book id
	}
	
	@Test
	public void testMakeBookWithDifferentBook()
	{
		//execute
		IBook book = sut_.makeBook(author2_, title2_, callNumber2_, id2_);
		
		//assert
		assertTrue(book.getID() == 2); //verifys that it returns the book id
	}
	
	@After
	public void tearDown() throws Exception{
	}
}