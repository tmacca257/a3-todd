package unitTesting;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.rules.ExpectedException;

import library.BorrowUC_CTL;
import library.BorrowUC_UI;
import library.daos.BookDAO;
import library.daos.BookHelper;
import library.daos.LoanHelper;
import library.daos.LoanMapDAO;
import library.daos.MemberHelper;
import library.daos.MemberMapDAO;
import library.interfaces.EBorrowState;
import library.interfaces.IBorrowUIListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.ILoanHelper;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.daos.IMemberHelper;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;


@RunWith (MockitoJUnitRunner.class)
public class TestBorrowUC_CTL {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Mock
	ICardReader reader_;
	
	@Mock
	IScanner scanner_;
	
	@Mock
	IPrinter printer_;
	
	@Mock
	IDisplay display_;
	
	@Mock
	BorrowUC_UI ui_;
	
	
	BorrowUC_CTL sut_;
	IBookHelper helper_;
	IMemberHelper memberHelper_;
	ILoanHelper loanHelper_;
	IBookDAO bookDAO_;
	ILoanDAO loanDAO_;
	IMemberDAO memberDAO_;
	EBorrowState state_;
	List<ILoan> loanList_;
	//JPanel jPanel_;
	
	
	@Before 
	public void setUp() throws Exception
	{
		helper_ = new BookHelper();
		memberHelper_ = new MemberHelper();
		loanHelper_ = new LoanHelper();
		bookDAO_ = new BookDAO(helper_);
		memberDAO_ = new MemberMapDAO(memberHelper_);
		loanDAO_ = new LoanMapDAO(loanHelper_);
		sut_ = new BorrowUC_CTL(reader_, scanner_, printer_, display_, bookDAO_, loanDAO_, memberDAO_, ui_);
		setupTestData();
	}
	
	@After
	public void tearDown() throws Exception{
	}
	
	@Test
	public void testInitialise() {
		//arrange
		
		//execute
		sut_.initialise();
		
		//assert
		verify(display_).getDisplay();
		verify(reader_).setEnabled(true);
		verify(scanner_).setEnabled(false);
		verify(ui_).setState(EBorrowState.INITIALIZED);
		//need to check setDisplay
		display_.setDisplay ((JPanel) ui_, "Borrow UI");
	}
	
	@Test
	public void testCardSwipedNoRestrictions()
	{
		//arrange
		sut_.initialise();
		
		//get member with no restrictions and no existing loans
		int memberID = 1;
				
		//execute
		sut_.cardSwiped(memberID);
		
		//assert
		assertEquals(sut_.getState(), EBorrowState.SCANNING_BOOKS);
		verify(reader_).setEnabled(false);
		verify(scanner_).setEnabled(true);
		verify(ui_).setState(EBorrowState.SCANNING_BOOKS);
		verify(ui_).displayScannedBookDetails("");
		verify(ui_).displayPendingLoan("");
		verify(ui_).displayMemberDetails(1, "fName0 lName0", "0001");
		verify(ui_).displayExistingLoan("");
		assertTrue(sut_.getScanCount() == 0);
	}
	
	@Test
	public void testCardSwipedOverDueLoans()
	{
		//arrange
		sut_.initialise();
		
		//select a member with ovedue loans
		int memberID = 2;
		
		//execute
		sut_.cardSwiped(memberID);
		
		//asserts
		assertEquals(sut_.getState(), EBorrowState.BORROWING_RESTRICTED);
		verify(ui_).setState(EBorrowState.BORROWING_RESTRICTED);
		verify(ui_).displayErrorMessage(String.format("Member %d cannot borrow at this time.", memberID));
		verify(ui_).displayMemberDetails(2, "fName1 lName1", "0002");
		verify(ui_).displayOverDueMessage();
		
	}
	
	@Test
	public void testScansCompletedWrongState()
	{
		//arrange
		sut_.initialise();
		
		exception.expect(RuntimeException.class);
		exception.expectMessage(containsString("BorrowUC_CTL : scansCompleted : illegal operation in state:"));
		
		//execute
		sut_.scansCompleted();
		
		//assert
		fail("should have thrown RuntimeException");
	}
	
	@Test
	public void testScanCompletedCorrectState()
	{
		//arrange
		sut_.initialise();
		
		//swipe the members card
		sut_.cardSwiped(1);
		
		//scan book
		sut_.bookScanned(7);
		
		
		//execute
		sut_.scansCompleted();
		
		//assert
		assertEquals(sut_.getState(), EBorrowState.CONFIRMING_LOANS);
		
	}
	
	
	public void setupTestData()
	{
		IBook[] book = new IBook[15];
		IMember[] member = new IMember[6];
		
		book[0]  = bookDAO_.addBook("author1", "title1", "callNo1");
		book[1]  = bookDAO_.addBook("author1", "title2", "callNo2");
		book[2]  = bookDAO_.addBook("author1", "title3", "callNo3");
		book[3]  = bookDAO_.addBook("author1", "title4", "callNo4");
		book[4]  = bookDAO_.addBook("author2", "title5", "callNo5");
		book[5]  = bookDAO_.addBook("author2", "title6", "callNo6");
		book[6]  = bookDAO_.addBook("author2", "title7", "callNo7");
		book[7]  = bookDAO_.addBook("author2", "title8", "callNo8");
		book[8]  = bookDAO_.addBook("author3", "title9", "callNo9");
		book[9]  = bookDAO_.addBook("author3", "title10", "callNo10");
		book[10] = bookDAO_.addBook("author4", "title11", "callNo11");
		book[11] = bookDAO_.addBook("author4", "title12", "callNo12");
		book[12] = bookDAO_.addBook("author5", "title13", "callNo13");
		book[13] = bookDAO_.addBook("author5", "title14", "callNo14");
		book[14] = bookDAO_.addBook("author5", "title15", "callNo15");
		
		member[0] = memberDAO_.addMember("fName0", "lName0", "0001", "email0");
		member[1] = memberDAO_.addMember("fName1", "lName1", "0002", "email1");
		member[2] = memberDAO_.addMember("fName2", "lName2", "0003", "email2");
		member[3] = memberDAO_.addMember("fName3", "lName3", "0004", "email3");
		member[4] = memberDAO_.addMember("fName4", "lName4", "0005", "email4");
		member[5] = memberDAO_.addMember("fName5", "lName5", "0006", "email5");
		
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
				
		//create a member with overdue loans		
		for (int i=0; i<2; i++) {
			ILoan loan = loanDAO_.createLoan(member[1], book[i]);
			loanDAO_.commitLoan(loan);
		}
		cal.setTime(now);
		cal.add(Calendar.DATE, ILoan.LOAN_PERIOD + 1);
		Date checkDate = cal.getTime();		
		loanDAO_.updateOverDueStatus(checkDate);
		
		//create a member with maxed out unpaid fines
		member[2].addFine(10.0f);
		
		//create a member with maxed out loans
		for (int i=2; i<7; i++) {
			ILoan loan = loanDAO_.createLoan(member[3], book[i]);
			loanDAO_.commitLoan(loan);
		}
		
		//a member with a fine, but not over the limit
		member[4].addFine(5.0f);
		
		//a member with a couple of loans but not over the limit
		for (int i=7; i<9; i++){
			ILoan loan = loanDAO_.createLoan(member[5], book[i]);
			loanDAO_.commitLoan(loan);
		}
	}

}
